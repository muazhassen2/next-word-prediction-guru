/**
 * @file Background script is executed when chrome browser
 * is opened and chrome extension is turned on.
 * @author Jan Jakub Kubik <jakupkubik@gmail.com>
 * @module background
 */


/**  URL for downloading trained neural network language model. */
const TRAINED_MODEL_URL = 'https://storage.googleapis.com/sequencepredictiontool/success/model.json';


/**  IndexedDB path for saving and loading trained neural network language model. */
const INDEXED_DB_PATH_FOR_TRAINED_MODEL = 'indexeddb://spt';


/** URL reference to lexicon. */
let LEXICON_URL = "https://storage.googleapis.com/sequencepredictiontool/success/lexicon.json";


/**  localStorage for saving and loading lexicon. */
const LOCALSTORAGE_PATH_FOR_LEXICON = 'lexicon';


/**
 * Download, save trained language model, lexicon and load them.
 */
async function save_lang_model_and_lexicon() {
    console.log('function save_lang_model_and_lexicon');
    console.log('===========================================');
    await download_and_save_lang_model(TRAINED_MODEL_URL, INDEXED_DB_PATH_FOR_TRAINED_MODEL);
    await download_and_save_lexicon(LEXICON_URL, LOCALSTORAGE_PATH_FOR_LEXICON);

    await load_lang_model_and_lexicon();
}


/**
 * Load saved language model and lexicon. Then create SequencePredictor object for sequence generation.
 * At the end create communication canal with this class for communication.
 */
async function load_lang_model_and_lexicon() {
    console.log('function load_lang_model_and_lexicon');
    console.log('===========================================');
    let model = await load_nn_model(INDEXED_DB_PATH_FOR_TRAINED_MODEL);
    let {id_to_sub_word, sub_word_to_id} = create_lexicon_mappers(LOCALSTORAGE_PATH_FOR_LEXICON);
    console.log(id_to_sub_word);

    const predictor = new SequencePredictor(model, id_to_sub_word, sub_word_to_id);
    create_communication_canal(predictor);
}

chrome.runtime.onInstalled.addListener(save_lang_model_and_lexicon);
chrome.runtime.onStartup.addListener(load_lang_model_and_lexicon);
