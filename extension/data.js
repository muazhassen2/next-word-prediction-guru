/**
 * @file this module contains function for operations with data.
 * Operations are downloading, saving and loading data. Data are
 * trained neural network and lexicon saved on remote server.
 * @author Jan Jakub Kubik <jakupkubik@gmail.com>
 * @module data
 */


/**
 * Download and save neural network model to indexed db.
 * @param {string} trained_model_url - ULR location of trained nn model
 * @param {string} indexed_db_path_for_trained_model - Path for saving download model
 */
async function download_and_save_lang_model(trained_model_url, indexed_db_path_for_trained_model){
    const startTime = performance.now();
    console.log('Downloading of pretrained model from url ', trained_model_url, ' ...');
    let model = await tf.loadLayersModel(trained_model_url);
    await model.save(indexed_db_path_for_trained_model);

    const totalTime = Math.floor(performance.now() - startTime);
    console.log(`Model is downloaded in ${totalTime} ms and saved to IndexedDB`);
}


/**
 * Load trained neural network model.
 * @param {string} indexed_db_path_for_trained_model - Indexed db path for loading nn model.
 * @return trained nn model
 */
async function load_nn_model(indexed_db_path_for_trained_model) {
    let model = null;
    try {
        console.log('Loading of pretrained model from indexed db ', indexed_db_path_for_trained_model, ' ...');
        model = await tf.loadLayersModel(indexed_db_path_for_trained_model);

    } catch (err) {
        console.log('Unable to load model from indexdb ', indexed_db_path_for_trained_model);
    }

    return model;
}


/**
 * Download and save lexicon to localStorage.
 * @param lexicon_url
 * @param localstorage_path_for_lexicon
 */
async function download_and_save_lexicon(lexicon_url, localstorage_path_for_lexicon) {
    console.log('Downloading lexicon from url ', lexicon_url, ' ...');
    let resp = await fetch(lexicon_url);
    let json_data = await resp.json();
    console.log('Saving downloaded lexicon to localStorage ', json_data);
    window.localStorage.setItem(localstorage_path_for_lexicon, JSON.stringify(json_data));

    return null
}


/**
 * Create mapping from lexicon of id to token and reverse.
 * @param localstorage_path_for_lexicon
 * @returns {{sub_word_to_id: {}, id_to_sub_word: {}}} - Dictionary with mapping created from lexicon.
 */
function create_lexicon_mappers(localstorage_path_for_lexicon) {
     /**
     * Create mapping ids to sub words from lexicon.
     * @param {Object} json_data - Object with parsed json data.
     * @returns {Object} - Object with mapping ids to sub words.
     */
    function get_ids_to_sub_words(json_data) {
        let ids_to_sub_words = {};
        for (let key in json_data) {
            ids_to_sub_words[parseInt(key, 10) + 1 ] = json_data[key];
        }
        console.log('ids_to_sub_words mapping: ', ids_to_sub_words);
        return ids_to_sub_words;
    }

    /**
     * Create mapping sub words to ids from lexicon.
     * @param {Object} json_data - Object with parsed json data.
     * @returns {Object} - Object with mapping sub words to ids.
     */
    function get_sub_words_to_ids(json_data) {
        let sub_words_to_ids = {};
        for (let key in json_data) {
            sub_words_to_ids[json_data[key]] = parseInt(key, 10) + 1;
        }
        console.log('sub_words_to_ids mapping: ', sub_words_to_ids);
        return sub_words_to_ids;
    }

    let data = window.localStorage.getItem(localstorage_path_for_lexicon);

    console.log('Loading lexicon from localStorage ', localstorage_path_for_lexicon, ' ...');
    let json_data = JSON.parse(data);

    /** Mapper of id to sub word. */
    let id_to_sub_word = get_ids_to_sub_words(json_data);
    /** Mapper of sub word to id. */
    let sub_word_to_id = get_sub_words_to_ids(json_data);

    return {
        id_to_sub_word,
        sub_word_to_id
    };
}
