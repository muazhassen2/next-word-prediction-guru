/**
 * @file SequencePredictor class for generating sequences
 * with usage of neural network LM.
 * @author Jan Jakub Kubik <jakupkubik@gmail.com>
 * @module sequence_predictor
 */


/**
 * This class is for using LM for sequences prediction.
 * Sequence is predicted according written text by user, that is asynchronously
 * sent to SequencePredictor by communication canal with additional data.
 */
class SequencePredictor {
    /**
     * Constructor for sequence predictor class. It initializes LM,
     * required properties and creates sub word tokenizer.
     * @constructor
     */
    constructor(model, id_to_sub_word, sub_word_to_id) {
        /** LM for sequence generation */
        this.model = model;

        /** Hidden state of neurons in GRU 1 recurrent layer of neural network BEFORE sequence prediction. */
        this.model_state_before_prediction_gru1 = null;

        /** Hidden state of neurons in GRU 2 recurrent layer of neural network BEFORE sequence prediction. */
        this.model_state_before_prediction_gru2 = null;

        /** Object fot sub word tokenization. */
        this.sub_word_tokenizer = new SubWordTokenizer(id_to_sub_word, sub_word_to_id);

        /** Ending characters for sentence. */
        this.sentence_ends = ['.', '!', '?'];
    }

    /**
     * Generate whole sequence with neural network using last word.
     * @param {string} input_text - Word that is tokenized and used for sequence prediction.
     * @param {number} max_len - Maximum length for sequence generation.
     * @param {boolean} greedy - use greedy algorithm.
     * @returns {string} - Predicted sequence.
     */
    generate_sequence(input_text, max_len, greedy) {
        // if input text is empty then return empty string
        // and don't use neural network for prediction
        if (input_text === '') {
            return ''
        }

        let tokens = this.sub_word_tokenizer.encode(input_text);

        // Iterate over all tokens from input string for getting
        // context of written text by changing hidden states in recurrent layers.
        for(let i = 0;i < tokens.length - 2;i++){
            this.predict_next_token_id(tokens[i], greedy);
        }

        // sequence prediction
        // predict token from last input token
        let predicted_token_id = this.predict_next_token_id(tokens[tokens.length-1], greedy);

        // After last token from input string (start of generation sequence) save hidden states.
        this.model_state_before_prediction_gru1 = this.model.layers[1].states[0].arraySync();
        this.model_state_before_prediction_gru2 = this.model.layers[3].states[0].arraySync();

        // decode first token in sequence
        let token = this.sub_word_tokenizer.decode([predicted_token_id['id']]);

        let nmb_of_words  = 0;
        let sequence = "";
        let seq = [];
        let probabilities = [];

        while (true) {
            // words are divide just by spaces
            if (token.slice(-1) === ' ') {
                nmb_of_words +=1;
            }

            seq.push(token);
            sequence += token;
            // save its probability
            probabilities.push(predicted_token_id['prob']);

            // check end of generated sequence
            if (this.ContainsAny(token, this.sentence_ends)) { break; }

            // generate new token
            predicted_token_id = this.predict_next_token_id(predicted_token_id['id'], greedy);
            token = this.sub_word_tokenizer.decode([predicted_token_id['id']]);

            // check word len of generated sequence
            if (nmb_of_words === max_len) {
                break
            }
        }

        let sum = 0;
        for (let prob of probabilities) {
            sum += Math.log10(prob);
        }
        let seq_prob = sum / probabilities.length;

        // sequence list, probablity list, probability for whole sequence
        console.log(seq, probabilities, Math.pow(10, seq_prob));
        if (seq_prob < Math.log10(0.5)) {  // 0.5: -0.3010299956639812, 0.7: -0.1549019599857432, 1: 0@
            return ''
        }

        return sequence; // predicted sequence
    }

    /**
     * Predict next token with neural network according token_id.
     * @param {number} token_id - Id of input token for prediction.
     * @param {boolean} greedy - use greedy algorithm.
     * @returns {number} - index of predicted token.
     */
    predict_next_token_id(token_id, greedy) {
        let predicted_vector = this.model.predict(tf.tensor1d([token_id], 'int32'));
        let probabilities = predicted_vector.dataSync();

        if (greedy) {
            return this.greedy(predicted_vector)
        }

        return this.sampling(probabilities);
    }

    /** Greedy algorithm for next token id generation (just for testing purpose) */
    greedy(probabilities_of_token_ids) {
        console.log('Greedy algorithm');
        return {
            'prob': probabilities_of_token_ids.max(2).dataSync()[0],
            'id': probabilities_of_token_ids.argMax(2).dataSync()[0],
        };
    }

    /** Pure sampling algorithm for next token id generation */
    sampling(probabilities_of_token_ids) {
        console.log('Sampling with probability algorithm.');
        return this.random_choice(probabilities_of_token_ids);
    }

    // numpy random choice for just one element (predicted token)
    // https://stackoverflow.com/questions/41654006/numpy-random-choice-in-javascript
    random_choice(p) {
        let rnd = p.reduce((a, b) => a + b) * Math.random();
        let prob = rnd;
        let lex_id = p.findIndex(a => (rnd -= a) < 0);

        return {
            'prob': prob,
            'id': lex_id,
        };
    }

    /**
     * Check if in input string is any of items.
     * @param {string} str - Input string for checking.
     * @param {Array} items - Elements to search for.
     * @returns {boolean} - true if any of elements is in input string otherwise false.
     */
    ContainsAny(str, items) {
        for (let item of items) {
            if (str.indexOf(item) > -1) {
                return true;
            }
        }
        return false;
    }
}
