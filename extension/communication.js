/**
 * @file communication.js contains function for creating communication
 * canal between content script and background script. Then generates
 * sequences by SequencePredictor object according data from content
 * script and sends generated sequences back to content script.
 * @author Jan Jakub Kubik <jakupkubik@gmail.com>
 * @module communication
 */


/**
 * Create asynchronous canal for communication between background and
 * content script for handling predict_sequence message from content script.
 * @param predictor - SequencePredictor class.
 */
function create_communication_canal(predictor) {
    chrome.runtime.onConnect.addListener(function(port) {
        console.assert(port.name === "com_canal_predict_sequence");
        port.onMessage.addListener(function(request) {

            if (request.action === 'predict_sequence') {

                if (predictor.model === null) {
                    console.log('Predictor is not initialized.');
                } else {

                    if (request.force_reset) {
                        console.log('New HTML element: force resetting LM hidden states.');
                        predictor.model.resetStates();

                    } else {
                        // If previous predicted sequence was not selected and if there
                        // is model_state_before_prediction reset hidden states of
                        // LM. It there is model_state_before_prediction
                        // set to null means that this is fist prediction and there it is nowhere
                        // to reset hidden state.
                        if (predictor.model_state_before_prediction_gru1 !== null &&
                            request.selected_sequence === false) {
                            console.log('Resetting hidden states to state before last sequence generation.');
                            predictor.model.layers[1].resetStates(tf.tensor(predictor.model_state_before_prediction_gru1));
                            predictor.model.layers[3].resetStates(tf.tensor(predictor.model_state_before_prediction_gru2));
                        }
                    }

                    // generate new sequence and send it back to content script
                    let greedy = false; // true = use greedy algorithm for tokens generation, false = use sampling with probabilities
                    let predicted_sequence = predictor.generate_sequence(request.word, parseInt(request.max_len), greedy);
                    port.postMessage({pred_sequence: predicted_sequence});
                }
            }
        });
    });
}
