# Sequence Prediction Tool (SPT)
SPT is for predicting sequence of words in Google Chrome browser. It is Chrome extension that
predicts sequence according written text by user in input, div or textarea HTML element. For 
prediction it uses trained recurrent neural network and lexicon that are saved on server.

## Install and run

**There are 3 options for using SPT:**
1. **Install Sequence Prediction Tool from [Chrome Web Store](https://chrome.google.com/webstore/detail/sequence-prediction-tool/biblhlpmjanngadiholiloifmnmmfohk)** 
2. **Adjusting extension part of SPT:**
    * Clone this repository.
    * Change files in extension subdirectory.
    * Install extension with trained neural network to Google Chrome browser.
    * Neural network is downloaded from server. 
    * _**Prerequisites**_: Google Chrome browser in developer mode and nothing else. 
    jQery, JqeryUI and TensorFlow.js are already included in directory  extension/external_modules.
3. **Changing neural network or extension part or both of SPT:**
    * Clone this repository.
    * Train neural network.
    * Save trained model and lexicon to the server.
    * Change TRAINED_MODEL_URL to saved model URL and LEXICON_URL to saved lexicon URL.
    * Install extension with trained neural network to Google Chrome browser. 
    * _**Prerequisites**_: Google Chrome browser in developer mode and Python 3.6 virtual
     environment with installed requirements (`pip install -r neural_network/requirements.txt`). 

## Demo web page
Installed extension can be used on any web page, but there is too demonstrative web page for trying SPT. 
It is accessible via home button from extension popup or directly https://sequencepredictiontool.com/.


## Author

**Jan Jakub Kubik ([jakupkubik@gmail.com](jakupkubik@gmail.com))**


## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
