#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Jan Jakub Kubik <jakupkubik@gmail.com>

from time import time

import tensorflow as tf
import tensorflowjs as tfjs

from dataset import Dataset


class RNN:
    """Recurrent neural network.

    Class for creating and training of recurrent neural network.

    """

    def __init__(
        self,
        gru_layers,
        batch_size,
        embedding_dim,
        rnn_units,
        dropout_rate,
        epochs,
        lex_dir,
        train_dataset_size,
    ):
        self.__epochs = epochs

        self.gru_layers = gru_layers
        self.embedding_dim = embedding_dim
        self.rnn_units = rnn_units
        self.dropout_rate = dropout_rate

        self.dataset = Dataset(train_dataset_size, batch_size, lex_dir)
        self.model = self.__create_rnn(
            gru_layers, batch_size, embedding_dim, rnn_units, dropout_rate
        )

    def __create_rnn(
        self, nmb_of_gru_layers, batch_size, embedding_dim, rnn_units, dropout_rate
    ):
        """Create recurrent neural network

        Returns:
            Created neural network model.

        """

        print("======" * 20)
        print(batch_size, nmb_of_gru_layers, embedding_dim, rnn_units, dropout_rate)
        print("======" * 20)
        model = tf.keras.Sequential()
        model.add(
            tf.keras.layers.Embedding(
                self.dataset.tokenizer.vocab_size,
                embedding_dim,
                batch_input_shape=[batch_size, None],
            )
        )

        for _ in range(nmb_of_gru_layers):
            model.add(
                tf.keras.layers.GRU(
                    rnn_units,
                    return_sequences=True,
                    stateful=True,
                    recurrent_initializer="glorot_uniform",
                )
            )
            model.add(tf.keras.layers.Dropout(dropout_rate))

        model.add(
            tf.keras.layers.Dense(
                units=self.dataset.tokenizer.vocab_size,
                activation=tf.keras.activations.softmax,
            )
        )
        return model

    def train(self, log_dir):
        """Setup optimizer, loss function for neural network training and train neural network."""
        self.model.summary()
        self.model.compile(
            optimizer="adam",
            loss=tf.keras.losses.sparse_categorical_crossentropy,
            metrics=["sparse_categorical_accuracy"],
        )
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir)

        self.model.fit(
            self.dataset.train_dataset,
            epochs=self.__epochs,
            validation_data=self.dataset.validate_dataset,
            callbacks=[tensorboard_callback],
        )

    def save_trained_rnn(self, path):
        """Save trained TensorFlow model.

        Batch size of trained neural network is changed due
        to compatibility for prediction (prediction is base on 1 input not 32).

        """
        model = self.__create_rnn(
            self.gru_layers, 1, self.embedding_dim, self.rnn_units, self.dropout_rate
        )
        old_weights = self.model.get_weights()
        model.set_weights(old_weights)
        model.save(path)
        self.model = model

    def convert_rnn_to_tfjs_and_save(self, path):
        """Convert trained TensorFlow model to TensorFlow.js model and save it."""
        print(self.model.summary())
        tfjs.converters.save_keras_model(self.model, path)
