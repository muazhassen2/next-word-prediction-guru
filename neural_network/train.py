#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Jan Jakub Kubik <jakupkubik@gmail.com>


from rnn import RNN


def train(
    nmb_of_gru_leyers,
    batch_size,
    embedding_dim,
    rnn_units,
    dropout_rate,
    epochs,
    tf_model_dir,
    tfjs_model_dir,
    train_dataset_size,
):
    print("Recurrent neural network training...")

    neural_network = RNN(
        gru_layers=nmb_of_gru_leyers,
        batch_size=batch_size,
        embedding_dim=embedding_dim,
        rnn_units=rnn_units,
        dropout_rate=dropout_rate,
        epochs=epochs,
        lex_dir=tfjs_model_dir,
        train_dataset_size=train_dataset_size,
    )

    neural_network.train(log_dir="logs/" + tfjs_model_dir.split("/")[1])

    print("\n# Evaluate on test data")
    results = neural_network.model.evaluate(neural_network.dataset.test_dataset)
    print("test loss, test acc:", results)

    neural_network.save_trained_rnn(path=tf_model_dir)
    neural_network.convert_rnn_to_tfjs_and_save(path=tfjs_model_dir)


def failed_training():
    train(
        nmb_of_gru_leyers=2,
        rnn_units=340,
        epochs=10,
        embedding_dim=200,
        dropout_rate=0.2,
        batch_size=32,
        tf_model_dir="models/fail/model.h5",
        tfjs_model_dir="models/fail/",
        train_dataset_size=30,
    )


def success_training():
    train(
        nmb_of_gru_leyers=2,
        rnn_units=340,
        epochs=29,
        embedding_dim=200,
        dropout_rate=0.1,
        batch_size=32,
        tf_model_dir="models/success/model.h5",
        tfjs_model_dir="models/success/",
        train_dataset_size=5,
    )


if __name__ == "__main__":
    # failed_training()
    success_training()
